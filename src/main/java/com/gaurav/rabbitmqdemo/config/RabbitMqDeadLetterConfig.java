package com.gaurav.rabbitmqdemo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqDeadLetterConfig {
    public static final String QUEUE="simple.queue2";
    public static final String QUEUE_KEY="simple.queue.key";
    public static final String EXCHANGE="simple.exchange";
    public static final String DEAD_QUEUE="dead.queue";
    public static final String DEAD_KEY="dead.key";
    public static final String DEAD_EXCHANGE="dead.exchange";

    @Bean
    Queue queue(){
        return QueueBuilder.durable(QUEUE).deadLetterExchange(DEAD_EXCHANGE).deadLetterRoutingKey(DEAD_KEY).build();
    }
/*    @Bean
    Queue queue() {
        return QueueBuilder.durable(QUEUE)
                .withArgument("x-dead-letter-exchange",
                        DEAD_EXCHANGE)
                .withArgument("x-dead-letter-routing-key",
                        DEAD_KEY)
                .build();
    }*/
    @Bean
    DirectExchange exchange(){
        return new DirectExchange(EXCHANGE);
    }
    @Bean
    Queue deadQueue(){
        return new Queue(DEAD_QUEUE);
    }
    @Bean
    DirectExchange deadExchange()
    {
        return new DirectExchange(DEAD_EXCHANGE);
    }
    @Bean
    Binding personBinding()
    {
        return BindingBuilder.bind(queue()).to(exchange()).with(QUEUE_KEY);
    }
    @Bean
    Binding deadBinding()
    {
        return BindingBuilder.bind(deadQueue()).to(deadExchange()).with(DEAD_KEY);
    }
}
