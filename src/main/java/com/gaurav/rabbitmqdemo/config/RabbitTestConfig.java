package com.gaurav.rabbitmqdemo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitTestConfig {
    @Bean
    Queue createQueue()
    {
        return new Queue("Test");
    }
    @Bean
    DirectExchange createExchange(){
        return new DirectExchange("TestExchange");
    }
    @Bean
    Binding createBinder(){
        return BindingBuilder.bind(createQueue()).to(createExchange()).with("testRabbit");
    }
}
