package com.gaurav.rabbitmqdemo.entities;
import lombok.*;
import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Person implements Serializable {
    private Integer id;
    private String name;
    private Integer age;
}
