package com.gaurav.rabbitmqdemo.services;

import com.gaurav.rabbitmqdemo.config.RabbitMqDeadLetterConfig;
import com.gaurav.rabbitmqdemo.entities.Person;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
@Service
public class PersonProducerService {
    private final RabbitTemplate rabbitTemplate;
    public PersonProducerService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
    public void producePerson(Person person) {
        rabbitTemplate.convertAndSend(RabbitMqDeadLetterConfig.EXCHANGE,
                RabbitMqDeadLetterConfig.QUEUE_KEY
                , person);
        System.out.println("Produced : " + person);
    }
}