package com.gaurav.rabbitmqdemo.services;

import com.gaurav.rabbitmqdemo.entities.Mail;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class MailConsumerService {
    @RabbitListener(queues = "mail.queue")
    public void consumeMail(Mail mail) {
        System.out.println(mail);
    }
}