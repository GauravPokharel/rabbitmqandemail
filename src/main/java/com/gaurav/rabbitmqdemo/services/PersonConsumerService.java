package com.gaurav.rabbitmqdemo.services;

import com.gaurav.rabbitmqdemo.config.RabbitMqDeadLetterConfig;
import com.gaurav.rabbitmqdemo.entities.Person;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class PersonConsumerService {
    @RabbitListener(queues = RabbitMqDeadLetterConfig.QUEUE)
    public void consumePerson(Person person) throws Exception {
        if (person.getAge() < 18) {
            System.out.println("You are not eligible : " +
                    person);
            throw new Exception();
        } else {
            System.out.println("You are eligible : " + person);
        }
    }
}