package com.gaurav.rabbitmqdemo.controllers;

import com.gaurav.rabbitmqdemo.entities.Mail;
import com.gaurav.rabbitmqdemo.services.MailProducerService;
import com.gaurav.rabbitmqdemo.services.MailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {
    private final MailProducerService mailProducerService;
    public MailController(MailProducerService mailProducerService){
        this.mailProducerService=mailProducerService;
    }

    @Autowired
    private MailSenderService mailSenderService;
    @GetMapping("/sendMail")
    public ResponseEntity<?> send(){
        Mail mail= Mail.builder()
                .to("gp@email.com")
                .subject("A test mail for rabbitmq.")
                .body("This is test mail for rabbitmq.")
                .build();
        for (int i = 0; i < 5; i++) {
            mail.setId(i);
            mailProducerService.produceMessage(mail);
        }
        return ResponseEntity.ok("Mail is sending to all the receivers.");
    }
    @GetMapping("/sendMailToGmail")
    public ResponseEntity<?> sendMail(){
        Mail mail = Mail.builder()
                .to("bpokharel077@gmail.com")
                .subject("Mail testing")
                .body("This is testing body")
                .from("pokharelgaurav0@gmail.com")
                .build();
        mailSenderService.sendEmail(mail);
        return ResponseEntity.ok("Mail is sending to all the receivers.");
    }
}
