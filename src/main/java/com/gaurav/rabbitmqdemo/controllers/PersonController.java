package com.gaurav.rabbitmqdemo.controllers;

import com.gaurav.rabbitmqdemo.entities.Person;
import com.gaurav.rabbitmqdemo.services.PersonProducerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    private final PersonProducerService service;
    public PersonController(PersonProducerService service) {
        this.service = service;
    }
    @GetMapping("/person/send")
    public String send() {
        for (int i = 13; i < 23; i++) {
            service.producePerson(Person.builder()
                    .id(i - 13 + 1)
                    .name("Random name")
                    .age(i)
                    .build());
        }
        return "All messages are produced successfully.";
    }
}
